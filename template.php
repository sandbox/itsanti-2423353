<?php
/**
 * @file
 * Theme functions for Lazy Theme.
 */

/**
 * Implements hook_theme().
 */
function lazy_theme_theme() {
  return array(
    'lazy_counter_cron_setup' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Display cron setup form in table.
 */
function lazy_theme_lazy_counter_cron_setup($variables) {
  $form = $variables['form'];
  $output = '';

  $rows = array(
    array(
      'data' => array(
        render($form['lazy_counter_reset_interval']),
        render($form['lazy_counter_get_prize']),
      ),
      'no_striping' => TRUE,
    ),
  );

  $output .= theme('table', array(
    'rows' => $rows,
    'attributes' => array('class' => 'lazy_theme_table'),
  ));

  $output .= drupal_render_children($form);
  return $output;
}
